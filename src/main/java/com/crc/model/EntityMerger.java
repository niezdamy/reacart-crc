/*
 * Copyright (c) 2018 Accenture
 * For purposes of Corporate Readiness Certificate Programme
 * All rights reserved
 */

package com.crc.model;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
/**
 * @author mpichura
 */
public class EntityMerger {

    public static <T extends Serializable> T mergeEntities(T origin, T received) throws IllegalAccessException {
        Class<?> clazz = origin.getClass();
        Object returnEntity = origin;
        for (Field field : clazz.getDeclaredFields()) {
            if (!(Modifier.isStatic(field.getModifiers()) || Modifier.isFinal(field.getModifiers()))) {
                field.setAccessible(true);
                Object value1 = field.get(origin);
                Object value2 = field.get(received);
                Object value = (value2 != null ) ? value2 : value1;
                field.set(returnEntity, value);
            }
        }
        return (T) returnEntity;
    }
}
