/*
 * Copyright (c) 2018 Accenture
 * For purposes of Corporate Readiness Certificate Programme
 * All rights reserved
 */

package com.crc.model;

import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
        import java.io.Serializable;
        import java.math.BigInteger;
/**
 * @author mpichura
 */
@Entity
@Table(name = "ITEM")
@DynamicUpdate
public class Item implements Serializable {
    private static final long serialVersionUID = -33333058586873479L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "ORDERED_QUANTITY")
    private BigInteger orderedQuantity;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "PRODUCT_ID")
    private Product product;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ORDER_ID")
    private Order order;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigInteger getOrderedQuantity() {
        return orderedQuantity;
    }

    public void setOrderedQuantity(BigInteger orderedQuantity) {
        this.orderedQuantity = orderedQuantity;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}
