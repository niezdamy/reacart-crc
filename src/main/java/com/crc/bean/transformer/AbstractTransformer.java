/*
 * Copyright (c) 2018 Accenture
 * For purposes of Corporate Readiness Certificate Programme
 * All rights reserved
 */

package com.crc.bean.transformer;

import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractTransformer<BEAN, MODEL extends Serializable> implements Transformer<BEAN, MODEL> {

    public List<BEAN> transformToBeans(List<MODEL> models){
        return models.stream().map(m -> transformToBean(m)).collect(Collectors.toList());
    }

    public List<MODEL> transformFromBeans(List<BEAN> beans){
        return beans.stream().map(b -> transformFromBean(b)).collect(Collectors.toList());
    }
}
