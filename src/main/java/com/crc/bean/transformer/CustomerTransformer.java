/*
 * Copyright (c) 2018 Accenture
 * For purposes of Corporate Readiness Certificate Programme
 * All rights reserved
 */

package com.crc.bean.transformer;

import com.crc.bean.CustomerBean;
import com.crc.model.Customer;
import org.springframework.stereotype.Component;
/**
 * @author mpichura
 */
@Component
//@Scope("prototype")
public class CustomerTransformer extends AbstractTransformer<CustomerBean, Customer> {


    @Override
    public CustomerBean transformToBean(Customer model) {
        return new CustomerBean.CustomerBuilder()
                .id(model.getId())
                .address(model.getAddress())
                .firstName(model.getFirstName())
                .lastName(model.getLastName())
                .name(model.getName())
                .phoneNb(model.getPhoneNb())
                .build();
    }

    @Override
    public Customer transformFromBean(CustomerBean bean) {
        Customer customer = new Customer();
        if (bean.getId() != null) {
            customer.setId(bean.getId());
        }
        customer.setFirstName(bean.getFirstName());
        customer.setLastName(bean.getLastName());
        customer.setAddress(bean.getAddress());
        customer.setName((bean.getName() != null ? bean.getName() : bean.getFirstName() + " " + bean.getLastName()));
        customer.setPhoneNb(bean.getPhoneNb());
        return customer;
    }
}
