/*
 * Copyright (c) 2018 Accenture
 * For purposes of Corporate Readiness Certificate Programme
 * All rights reserved
 */

package com.crc.bean.transformer;

import java.io.Serializable;
/**
 * @author mpichura
 */
public interface Transformer <BEAN, MODEL extends Serializable> {
    BEAN transformToBean ( MODEL model);
    MODEL transformFromBean(BEAN bean);
}
