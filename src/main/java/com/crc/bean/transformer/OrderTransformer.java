/*
 * Copyright (c) 2018 Accenture
 * For purposes of Corporate Readiness Certificate Programme
 * All rights reserved
 */

package com.crc.bean.transformer;

import com.crc.bean.OrderBean;
import com.crc.model.Order;
import org.springframework.stereotype.Component;
/**
 * @author mpichura
 */
@Component
//@Scope("prototype")
public class OrderTransformer extends AbstractTransformer<OrderBean, Order> {

    @Override
    public OrderBean transformToBean (Order model) {
        OrderBean orderBean = new OrderBean();
        orderBean.setId(model.getId());
        orderBean.setAddress(model.getAddress());
//        orderBean.setCustomer(customerTransformer.transformToBean(model.getCustomer()));
        orderBean.setStatus(model.getStatus());
//        orderBean.setItems(itemTransformer.transformToBeans(model.getItems()));
        orderBean.setTotalAmount(model.getTotalAmount());
        return orderBean;
    }

    @Override
    public Order transformFromBean(OrderBean bean) {
        Order order = new Order();
        if (bean.getId() != null) {
            order.setId(bean.getId());
        }
        order.setAddress(bean.getAddress());
//        order.setCustomer(customerTransformer.transformFromBean(bean.getCustomer()));
        order.setStatus(bean.getStatus());
//        order.getItems().addAll(itemTransformer.transformFromBeans(bean.getItems()));
        order.setTotalAmount(bean.getTotalAmount());
        return order;
    }
}

