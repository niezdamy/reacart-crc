/*
 * Copyright (c) 2018 Accenture
 * For purposes of Corporate Readiness Certificate Programme
 * All rights reserved
 */

package com.crc.bean.transformer;

import com.crc.bean.ItemBean;
import com.crc.model.Item;
import org.springframework.stereotype.Component;
/**
 * @author mpichura
 */
@Component
//@Scope("prototype")
public class ItemTransformer extends AbstractTransformer<ItemBean, Item> {

    @Override
    public ItemBean transformToBean(Item model) {
        ItemBean item = new ItemBean();
        item.setId(model.getId());
        item.setOrderedQuantity(model.getOrderedQuantity());
//        item.setOrder(orderTransformer.transformToBean(model.getOrder()));
//        item.setProduct(productTransformer.transformToBean(model.getProduct()));
        return item;
    }

    @Override
    public Item transformFromBean(ItemBean bean) {
        Item item = new Item();
        if (bean.getId() != null) {
            item.setId(bean.getId());
        }
        item.setOrderedQuantity(bean.getOrderedQuantity());
//        if(bean.getOrder() != null) {
//            item.setOrder(orderTransformer.transformFromBean(bean.getOrder()));
//        }
//        if (bean.getProduct() != null) {
//            item.setProduct(productTransformer.transformFromBean(bean.getProduct()));
//        }
        return item;
    }
}
