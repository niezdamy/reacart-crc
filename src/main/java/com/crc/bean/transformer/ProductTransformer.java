/*
 * Copyright (c) 2018 Accenture
 * For purposes of Corporate Readiness Certificate Programme
 * All rights reserved
 */

package com.crc.bean.transformer;

import com.crc.bean.ProductBean;
import com.crc.model.Product;
import org.springframework.stereotype.Component;
/**
 * @author mpichura
 */
@Component
//@Scope("prototype")
public class ProductTransformer extends AbstractTransformer<ProductBean, Product>{

    @Override
    public ProductBean transformToBean(Product model) {
        ProductBean productBean = new ProductBean();
        productBean.setId(model.getId());
        productBean.setName(model.getName());
        productBean.setAvailableQuantity(model.getAvailableQuantity());
        productBean.setPrice(model.getPrice());
        return productBean;
    }

    @Override
    public Product transformFromBean(ProductBean bean) {
        Product product = new Product();
        if (bean.getId() != null) {
            product.setId(bean.getId());
        }
        product.setName(bean.getName());
        product.setAvailableQuantity(bean.getAvailableQuantity());
        product.setPrice(bean.getPrice());
        return product;
    }
}
