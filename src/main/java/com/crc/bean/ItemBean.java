/*
 * Copyright (c) 2018 Accenture
 * For purposes of Corporate Readiness Certificate Programme
 * All rights reserved
 */

package com.crc.bean;

import java.math.BigInteger;
/**
 * @author mpichura
 */
public class ItemBean {
    private Long id;
    private BigInteger orderedQuantity;
    private ProductBean product;
    private OrderBean order;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigInteger getOrderedQuantity() {
        return orderedQuantity;
    }

    public void setOrderedQuantity(BigInteger orderedQuantity) {
        this.orderedQuantity = orderedQuantity;
    }

    public ProductBean getProduct() {
        return product;
    }

    public void setProduct(ProductBean product) {
        this.product = product;
    }

    public OrderBean getOrder() {
        return order;
    }

    public void setOrder(OrderBean order) {
        this.order = order;
    }

}
