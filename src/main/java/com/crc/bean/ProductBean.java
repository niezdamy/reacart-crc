/*
 * Copyright (c) 2018 Accenture
 * For purposes of Corporate Readiness Certificate Programme
 * All rights reserved
 */

package com.crc.bean;

import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
/**
 * @author mpichura
 */
public class ProductBean {
    private Long id;
    private String name;
    private BigDecimal price;
    private BigInteger availableQuantity;
    private List<ItemBean> items;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigInteger getAvailableQuantity() {
        return availableQuantity;
    }

    public void setAvailableQuantity(BigInteger availableQuantity) {
        this.availableQuantity = availableQuantity;
    }

    public List<ItemBean> getItems() {
        if (CollectionUtils.isEmpty(items)) {
            return new ArrayList<>();
        }
        return items;
    }

    public void setItems(List<ItemBean> items) {
        this.items = items;
    }

}
