/*
 * Copyright (c) 2018 Accenture
 * For purposes of Corporate Readiness Certificate Programme
 * All rights reserved
 */

package com.crc.bean;

import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
/**
 * @author mpichura
 */
public class CustomerBean {

    public CustomerBean(){};

    private CustomerBean (CustomerBuilder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.address = builder.address;
        this.phoneNb = builder.phoneNb;
        this.orders = builder.orders;
    }

    private Long id;
    private String name;
    private String firstName;
    private String lastName;
    private String address;
    private Long phoneNb;
    private List<OrderBean> orders;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getPhoneNb() {
        return phoneNb;
    }

    public void setPhoneNb(Long phoneNb) {
        this.phoneNb = phoneNb;
    }

    public List<OrderBean> getOrders() {
        if (CollectionUtils.isEmpty(orders)){
            orders = new ArrayList<>();
        }
        return orders;
    }

    public void setOrders(List<OrderBean> orders) {
        this.orders = orders;
    }

    public static class CustomerBuilder {
        private Long id;
        private String name;
        private String firstName;
        private String lastName;
        private String address;
        private Long phoneNb;
        private List<OrderBean> orders;

        public CustomerBuilder name (String name) {
            this.name = name;
            return this;
        }

        public CustomerBuilder id (Long id) {
            this.id = id;
            return this;
        }

        public CustomerBuilder firstName (String firstName) {
            this.firstName = firstName;
            return this;
        }

        public CustomerBuilder lastName (String lastName) {
            this.lastName = lastName;
            return this;
        }

        public CustomerBuilder address (String address) {
            this.address = address;
            return this;
        }

        public CustomerBuilder phoneNb (Long phoneNb) {
            this.phoneNb = phoneNb;
            return this;
        }

        public CustomerBuilder orders (List<OrderBean> orders) {
            this.orders = orders;
            return this;
        }

        public CustomerBean build () {
            return new CustomerBean(this);
        }
    }
}
