/*
 * Copyright (c) 2018 Accenture
 * For purposes of Corporate Readiness Certificate Programme
 * All rights reserved
 */

package com.crc.bean;
/**
 * @author mpichura
 */
public enum OrderStatus {
    CANCELLED,
    IN_CART,
    COMPLETED,
    CONFIRMED,
    PENDING
}
