/*
 * Copyright (c) 2018 Accenture
 * For purposes of Corporate Readiness Certificate Programme
 * All rights reserved
 */

package com.crc.bean;

import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
/**
 * @author mpichura
 */
public class  OrderBean {
    private Long id;
    private String address;
    private OrderStatus status;
    private BigDecimal totalAmount;
    private CustomerBean customer;
    private List<ItemBean> items;
    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public CustomerBean getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerBean customer) {
        this.customer = customer;
    }

    public List<ItemBean> getItems() {
        if (CollectionUtils.isEmpty(items)) {
            items = new ArrayList<>();
        }
        return items;
    }

    public void setItems(List<ItemBean> items) {
        this.items = items;
    }
}
