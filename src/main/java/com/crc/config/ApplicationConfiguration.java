/*
 * Copyright (c) 2018 Accenture
 * For purposes of Corporate Readiness Certificate Programme
 * All rights reserved
 */

package com.crc.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
/**
 * @author mpichura
 */
@Configuration
public class ApplicationConfiguration {

    Logger logger = LoggerFactory.getLogger(ApplicationConfiguration.class);

}
