/*
 * Copyright (c) 2018 Accenture
 * For purposes of Corporate Readiness Certificate Programme
 * All rights reserved
 */

package com.crc.dao;

import com.crc.model.Item;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
/**
 * @author mpichura
 */
public interface ItemDao extends CrudRepository<Item, Long> {
    List<Item> findByOrder(Long orderId);
}
