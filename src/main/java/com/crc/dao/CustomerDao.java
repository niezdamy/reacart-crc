/*
 * Copyright (c) 2018 Accenture
 * For purposes of Corporate Readiness Certificate Programme
 * All rights reserved
 */

package com.crc.dao;

import com.crc.model.Customer;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
/**
 * @author mpichura
 */
public interface CustomerDao extends CrudRepository<Customer, Long> {

    @Query("select c from Customer c where (:id is null or c.id = :id)" +
            "and (:name is null or lower(c.name) like '%'||lower(:name)||'%')" +
            "and (:firstName is null or lower(c.firstName) like '%'||lower(:firstName)||'%')" +
            "and (:lastName is null or lower(c.lastName) like '%'||lower(:lastName)||'%')" +
            "and (:phoneNb is null or c.phoneNb = :phoneNb)" +
            "and (:address is null or lower(c.address) like '%'||lower(:address)||'%')"
    )
    List<Customer> findByAllOptional(@Param("id") Long id,
                                     @Param("name") String name,
                                     @Param("firstName") String firstName,
                                     @Param("lastName") String lastName,
                                     @Param("phoneNb") Long phoneNb,
                                     @Param("address") String address);

}
