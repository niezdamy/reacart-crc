/*
 * Copyright (c) 2018 Accenture
 * For purposes of Corporate Readiness Certificate Programme
 * All rights reserved
 */

package com.crc.dao;

import com.crc.bean.OrderStatus;
import com.crc.model.Order;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
/**
 * @author mpichura
 */
public interface OrderDao extends CrudRepository<Order, Long> {
    List<Order> findByStatus(OrderStatus status);
    List<Order> findByCustomer(Long customerId);
    Order findByIdAndStatus(Long orderId, OrderStatus status);
    List<Order> findByCustomerAndStatus(Long customerId, OrderStatus status);
}
