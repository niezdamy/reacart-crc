/*
 * Copyright (c) 2018 Accenture
 * For purposes of Corporate Readiness Certificate Programme
 * All rights reserved
 */

package com.crc.dao;

import com.crc.model.Product;
import org.springframework.data.repository.CrudRepository;

import java.math.BigDecimal;
import java.util.List;
/**
 * @author mpichura
 */
public interface ProductDao extends CrudRepository<Product, Long> {
    List<Product> findByNameIgnoreCase(String name);
    List<Product> findByNameContainingIgnoreCase(String name);
    List<Product> findByPriceBetween(BigDecimal min, BigDecimal max);
    List<Product> findByPriceLessThan(BigDecimal max);
    List<Product> findByPriceGreaterThan(BigDecimal min);
}
