/*
 * Copyright (c) 2018 Accenture
 * For purposes of Corporate Readiness Certificate Programme
 * All rights reserved
 */

package com.crc.web;

import com.crc.bean.ProductBean;
import com.crc.service.ProductService;
import com.crc.service.TransformerProvider;
import org.hibernate.ObjectNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;
/**
 * @author mpichura
 */
@RestController
public class ProductController {

    Logger logger = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    ProductService productService;

    @Autowired
    TransformerProvider transformerProvider;

    @RequestMapping(value = "/web/product/{id}", method = RequestMethod.GET)
    public ProductBean getProductById(@PathVariable Long id) {
        return transformerProvider.setProductFromModel(productService.getProductById(id));
    }

    @RequestMapping(value = "/web/product/{id}", method = RequestMethod.DELETE)
    public void removeProduct(@PathVariable Long id) {
        productService.removeProduct(id);
    }

    @RequestMapping(value = "/web/product", method = RequestMethod.GET)
    public List<ProductBean> getProductsByName(@RequestParam(name = "name") String name) {
        return productService.getProductsByName(name).stream().map(p -> transformerProvider.setProductFromModel(p)).collect(Collectors.toList());
    }

    @RequestMapping(value = "/web/product/name-contains/{value}", method = RequestMethod.GET)
    public List<ProductBean> getProductsByNameContaining(@PathVariable String value) {
        return productService.getProductsByNameContaining(value).stream().map(p -> transformerProvider.setProductFromModel(p)).collect(Collectors.toList());
    }

    @RequestMapping(value = "/web/products", method = RequestMethod.GET)
    public List<ProductBean> getAllProducts() {
        return productService.getAllProducts().stream().map(p -> transformerProvider.setProductFromModel(p)).collect(Collectors.toList());
    }

    @RequestMapping(value = "/web/products/price", method = RequestMethod.GET)
    public List<ProductBean> getProductsByPrice(@RequestParam(name = "max", required = false) BigDecimal max,
                                                @RequestParam(name = "min", required = false) BigDecimal min) {
        return productService.getProductsByPrice(min, max).stream().map(p -> transformerProvider.setProductFromModel(p)).collect(Collectors.toList());
    }

    @RequestMapping(value = "/web/product", method = RequestMethod.POST)
    public Long addProduct(@RequestParam(name = "name") String name,
                           @RequestParam(name = "availableQty", defaultValue = "0") BigInteger availableQty,
                           @RequestParam(name = "price", defaultValue = "0") BigDecimal price) {
        ProductBean productBean = new ProductBean();
        productBean.setName(name);
        productBean.setAvailableQuantity(availableQty);
        productBean.setPrice(price);
        return productService.addProduct(transformerProvider.setProductFromBean(productBean));
    }

    @RequestMapping(value = "/web/product", method = {RequestMethod.PUT, RequestMethod.PATCH})
    public void updateProduct(@RequestParam(name = "id") Long id,
                              @RequestParam(name = "name", required = false) String name,
                              @RequestParam(name = "price", required = false) BigDecimal price,
                              @RequestParam(name = "availableQty", required = false) BigInteger availableQty)
            throws ObjectNotFoundException, IllegalAccessException {
        ProductBean productBean = new ProductBean();
        productBean.setId(id);
        productBean.setName(name);
        productBean.setAvailableQuantity(availableQty);
        productBean.setPrice(price);
        productService.updateProduct(transformerProvider.setProductFromBean(productBean));
    }


}
