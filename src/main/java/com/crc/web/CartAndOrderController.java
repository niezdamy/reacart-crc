/*
 * Copyright (c) 2018 Accenture
 * For purposes of Corporate Readiness Certificate Programme
 * All rights reserved
 */

package com.crc.web;

import com.crc.bean.ItemBean;
import com.crc.bean.OrderBean;
import com.crc.bean.OrderStatus;
import com.crc.model.Item;
import com.crc.model.Order;
import com.crc.service.CartAndOrderService;
import com.crc.service.ProductService;
import com.crc.service.TransformerProvider;
import javassist.NotFoundException;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
/**
 * @author mpichura
 */
@RestController
public class CartAndOrderController {

    @Autowired
    CartAndOrderService cartAndOrderService;

    @Autowired
    ProductService productService;

    @Autowired
    TransformerProvider transformerProvider;
    @GetMapping(value = {"/web/carts", "/web/orders"})
    public List<OrderBean> getOrders(HttpServletRequest request) {
        if (request.getServletPath().contains("cart")) {
            return cartAndOrderService.getOrdersByStatus(OrderStatus.IN_CART).stream().map(o -> transformerProvider.setOrderFromModel(o)).collect(Collectors.toList());
        } else {
            return cartAndOrderService.getOrdersOnly().stream().map(o -> transformerProvider.setOrderFromModel(o)).collect(Collectors.toList());
        }
    }

    @GetMapping(value = "/web/orders/{status}")
    public List<OrderBean> getOrdersByStatus(@PathVariable OrderStatus status) {
        return cartAndOrderService.getOrdersByStatus(status).stream().map(o -> transformerProvider.setOrderFromModel(o)).collect(Collectors.toList());
    }

    @GetMapping(value = "/web/orders/customer/{id}/orders")
    public List<OrderBean> getOrdersByStatus(@PathVariable Long id,
                                             @RequestParam(name = "status", required = false) OrderStatus status) {
        return cartAndOrderService.getOrdersByCustomerAndStatus(id, status).stream().map(o -> transformerProvider.setOrderFromModel(o)).collect(Collectors.toList());
    }

    @RequestMapping(value = {"/web/cart/item", "/web/order/item"},
            method = RequestMethod.POST)
    public Map<String, Long> addItem(@RequestParam(name = "productId") Long productId,
                                     @RequestParam(name = "quantity") BigInteger quantity,
                                     @RequestParam(name = "cartId", required = false) Long cartId,
                                     @RequestParam(name = "orderId", required = false) Long orderId,
                                     HttpServletRequest request) throws NotFoundException {
        Item item = createItem(quantity, null);
        OrderStatus status = null;
        if (request.getServletPath().contains("cart")) {
            status = OrderStatus.IN_CART;
            enrichItem(item, productId, cartId);
        } else {
            enrichItem(item, productId, orderId);
        }
        return cartAndOrderService.addItem(item, status);
    }

    @RequestMapping(value = {"/web/cart/items", "/web/order/items"},
            method = RequestMethod.DELETE)
    public void removeItems(@RequestParam(name = "itemId") Long[] itemIds) {
        cartAndOrderService.removeItemsFromCart(itemIds);
    }

    @RequestMapping(value = {"/web/cart/item/{id}", "/web/order/item/{id}"},
            method = {RequestMethod.PUT, RequestMethod.PATCH})
    public void updateItemQuantity(@PathVariable Long id,
                                   @RequestParam(name = "quantity") BigInteger quantity)
            throws ObjectNotFoundException, IllegalAccessException, NotFoundException {
        Item item = createItem(quantity, id);
        cartAndOrderService.updateItemInCart(item);
    }

    @RequestMapping(value = "/web/cart/{id}",
            method = {RequestMethod.PUT, RequestMethod.PATCH})
    public Long setOrderFromCart(@PathVariable Long id,
                                 @RequestParam(name = "customerId") Long customerId,
                                 @RequestParam(name = "status", required = false) OrderStatus status)
            throws ObjectNotFoundException, NotFoundException {
        return cartAndOrderService.setOrderFromCart(id, customerId, status);
    }

    @RequestMapping(value = "/web/order/{id}",
            method = {RequestMethod.PUT, RequestMethod.PATCH})
    public void updateOrder(@PathVariable Long id,
                            @RequestParam(name = "customerId", required = false) Long customerId,
                            @RequestParam(name = "status", required = false) OrderStatus status)
            throws ObjectNotFoundException, IllegalAccessException {
        OrderBean orderBean = new OrderBean();
        orderBean.setId(id);
        orderBean.setStatus(status);
        Order order = transformerProvider.setOrderFromBean(orderBean);
        cartAndOrderService.updateOrder(order, customerId);
    }

    private void enrichItem(Item item, Long productId, Long orderId) {
        if (productId != null) {
            item.setProduct(productService.getProductById(productId));
        }
        if (orderId != null) {
            item.setOrder(cartAndOrderService.getCartById(orderId));
        }
    }

    private Item createItem(BigInteger quantity, Long id) {
        ItemBean itemBean = new ItemBean();
        itemBean.setId(id);
        itemBean.setOrderedQuantity(quantity);
        Item item = transformerProvider.setItemFromBean(itemBean);
        return item;
    }
}
