/*
 * Copyright (c) 2018 Accenture
 * For purposes of Corporate Readiness Certificate Programme
 * All rights reserved
 */

package com.crc.web;

import com.crc.bean.CustomerBean;
import com.crc.service.CustomerService;
import com.crc.service.TransformerProvider;
import org.hibernate.ObjectNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
/**
 * @author mpichura
 */
@RestController
public class CustomerController {
    Logger logger = LoggerFactory.getLogger(CustomerController.class);

    @Autowired
    CustomerService customerService;

    @Autowired
    TransformerProvider transformerProvider;

    @RequestMapping(value = "/web/customer/{id}", method = RequestMethod.GET)
    public CustomerBean getCustomerById(@PathVariable Long id) {
        return transformerProvider.setCustomerFromModel(customerService.getCustomerById(id));
    }

    @RequestMapping(value = "/web/customers", method = RequestMethod.GET)
    public List<CustomerBean> getCustomers() {
        return customerService.getAllCustomers().stream().map(c -> transformerProvider.setCustomerFromModel(c)).collect(Collectors.toList());
    }

    @RequestMapping(value = "/web/customers/search", method = RequestMethod.GET)
    public List<CustomerBean> searchCustomers(@RequestParam(name = "id", required = false) Long id,
                                              @RequestParam(name = "name", required = false) String name,
                                              @RequestParam(name = "firstName", required = false) String firstName,
                                              @RequestParam(name = "lastName", required = false) String lastName,
                                              @RequestParam(name = "address", required = false) String address,
                                              @RequestParam(name = "phoneNb", required = false) Long phoneNb) {
        return customerService.searchCustomers(id, name, firstName, lastName, phoneNb, address).stream().map(c -> transformerProvider.setCustomerFromModel(c)).collect(Collectors.toList());
    }

    @RequestMapping(value = "/web/customer/{id}", method = RequestMethod.DELETE)
    public void removeCustomerById(@PathVariable Long id) {
        customerService.removeCustomer(id);
    }

    @RequestMapping(value = "/web/customer", method = RequestMethod.POST)
    public Long addCustomer(@RequestParam(name = "name") String name,
                            @RequestParam(name = "firstName", required = false) String firstName,
                            @RequestParam(name = "lastName", required = false) String lastName,
                            @RequestParam(name = "address", required = false) String address,
                            @RequestParam(name = "phoneNb", required = false) Long phoneNb) {
        CustomerBean customer = new CustomerBean.CustomerBuilder()
                .address(address)
                .firstName(firstName)
                .lastName(lastName)
                .name(name)
                .phoneNb(phoneNb)
                .build();
        return customerService.addCustomer(transformerProvider.setCustomerFromBean(customer));
    }

    @RequestMapping(value = "/web/customer", method = {RequestMethod.PUT, RequestMethod.PATCH})
    public void updateCustomer(@RequestParam(name = "name", required = false) String name,
                               @RequestParam(name = "firstName", required = false) String firstName,
                               @RequestParam(name = "lastName", required = false) String lastName,
                               @RequestParam(name = "address", required = false) String address,
                               @RequestParam(name = "phoneNb", required = false) Long phoneNb,
                               @RequestParam(name = "id") Long id)
            throws ObjectNotFoundException, IllegalAccessException {
        CustomerBean customer = new CustomerBean.CustomerBuilder()
                .id(id)
                .address(address)
                .firstName(firstName)
                .lastName(lastName)
                .name(name)
                .phoneNb(phoneNb)
                .build();
        customerService.updateCustomer(transformerProvider.setCustomerFromBean(customer));
    }

}
