/*
 * Copyright (c) 2018 Accenture
 * For purposes of Corporate Readiness Certificate Programme
 * All rights reserved
 */

package com.crc.service;

import com.crc.dao.CustomerDao;
import com.crc.model.Customer;
import com.crc.model.EntityMerger;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
/**
 * @author mpichura
 */
@Service
public class CustomerService {
    @Autowired
    CustomerDao customerDao;

    public Customer getCustomerById(Long id) {
        return customerDao.findOne(id);
    }

    public List<Customer> getAllCustomers(){
        Iterable<Customer> customers = customerDao.findAll();
        return StreamSupport.stream(customers.spliterator(), false).collect(Collectors.toList());
    }

    public List<Customer> searchCustomers(Long id, String name, String firstName, String lastName, Long phoneNb, String address){
        Iterable<Customer> customers = customerDao.
                findByAllOptional(id, name, firstName, lastName, phoneNb, address);
        return StreamSupport.stream(customers.spliterator(), false).collect(Collectors.toList());
    }

    public Long addCustomer(Customer customer) {
        customerDao.save(customer);
        return customer.getId();
    }

    @SuppressWarnings("unchecked")
    public void updateCustomer(Customer customer) throws ObjectNotFoundException, IllegalAccessException {
        Customer existingCustomer = getCustomerById(customer.getId());
        if (existingCustomer == null) {
            throw new ObjectNotFoundException (customer, "No customer with id: " + customer.getId() + " to update!");
        }
        EntityMerger.mergeEntities(existingCustomer, customer);
        customerDao.save(existingCustomer);
    }

    public void removeCustomer(Customer customer) {
        customerDao.delete(customer);
    }

    public void removeCustomer(Long id) {
        customerDao.delete(id);
    }

}
