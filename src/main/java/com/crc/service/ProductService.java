/*
 * Copyright (c) 2018 Accenture
 * For purposes of Corporate Readiness Certificate Programme
 * All rights reserved
 */

package com.crc.service;

import com.crc.dao.ProductDao;
import com.crc.model.EntityMerger;
import com.crc.model.Product;
import org.hibernate.ObjectNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
/**
 * @author mpichura
 */
@Service
public class ProductService {

    private Logger logger = LoggerFactory.getLogger(ProductService.class);

    @Autowired
    private ProductDao productDao;

    public Product getProductById(Long id) {
        return productDao.findOne(id);
    }

    public List<Product> getProductsByName(String name) {
        return productDao.findByNameIgnoreCase(name);
    }

    public List<Product> getProductsByNameContaining(String name) {
        return productDao.findByNameContainingIgnoreCase(name);
    }

    public List<Product> getAllProducts(){
        Iterable<Product> products = productDao.findAll();
        return StreamSupport.stream(products.spliterator(), false).collect(Collectors.toList());
    }

    public List<Product> getProductsByPrice(BigDecimal min, BigDecimal max){
        Iterable<Product> products;
        if (max != null && min == null) {
            products = productDao.findByPriceLessThan(max);
        } else if (max == null && min != null) {
            products = productDao.findByPriceGreaterThan(min);
        } else {
            products = productDao.findByPriceBetween(min, max);
        }
        return StreamSupport.stream(products.spliterator(), false).collect(Collectors.toList());
    }

    public Long addProduct(Product product) {
        productDao.save(product);
        return product.getId();
    }

    public void updateProduct(Product product) throws ObjectNotFoundException, IllegalAccessException {
        Product existingProduct = getProductById(product.getId());
        if (existingProduct == null) {
            logger.warn("No product to update!");
            throw new ObjectNotFoundException (product, "No product with id: " + product.getId() + " to update!");
        }
        EntityMerger.mergeEntities(existingProduct, product);
        productDao.save(existingProduct);
    }

    public void removeProduct(Product product) {
        productDao.delete(product);
    }

    public void removeProduct(Long id) {
        productDao.delete(id);
    }

}
