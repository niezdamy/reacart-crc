/*
 * Copyright (c) 2018 Accenture
 * For purposes of Corporate Readiness Certificate Programme
 * All rights reserved
 */

package com.crc.service;

import com.crc.bean.OrderStatus;
import com.crc.dao.CustomerDao;
import com.crc.dao.ItemDao;
import com.crc.dao.OrderDao;
import com.crc.model.Customer;
import com.crc.model.EntityMerger;
import com.crc.model.Item;
import com.crc.model.Order;
import javassist.NotFoundException;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
/**
 * @author mpichura
 */
@Service
public class CartAndOrderService {

    @Autowired
    OrderDao orderDao;

    @Autowired
    ItemDao itemDao;

    @Autowired
    CustomerDao customerDao;

    public Order getCartById(Long id) {
        return orderDao.findByIdAndStatus(id, OrderStatus.IN_CART);
    }

    public Order getOrderById(Long id) {
        return orderDao.findOne(id);
    }

    public Item getItemById(Long id) {
         return itemDao.findOne(id);
    }

    public List<Order> getOrdersByStatus(OrderStatus status){
        return orderDao.findByStatus(status);
    }

    public List<Order> getOrdersOnly(){
        return StreamSupport.stream(orderDao.findAll().spliterator(), false)
                .filter(o -> o.getStatus() != OrderStatus.IN_CART)
                .collect(Collectors.toList());
    }

    public List<Order> getOrdersByCustomerAndStatus(Long customerId, OrderStatus status){
        if(status != null) {
            return orderDao.findByCustomer(customerId);
        } else {
            return orderDao.findByCustomerAndStatus(customerId, status);
        }
    }

    public Map<String, Long> addItem(Item item, OrderStatus status) throws NotFoundException {
        if (item.getProduct().getId() == null) {
            throw new NotFoundException("No productId provided for Item!");
        }
        Map <String, Long> response = new HashMap<>();
        if (item.getOrder() == null || item.getOrder().getId() == null) {
            Order order = createOrder(status);
            orderDao.save(order);
            item.setOrder(order);
        }
        itemDao.save(item);
        response.put("cartId", item.getOrder().getId());
        response.put("itemId", item.getId());
        return response;
    }

    public Map<String, List<Long>> addItemsToCart(List<Item> items) throws NotFoundException {
        List<Long> itemIds = new ArrayList<>();
        Map <String, List<Long>> response = new HashMap<>();
        Long orderId = null;
        Order order = null;
        for (Item item : items) {
            if (item.getProduct().getId() == null) {
                throw new NotFoundException("No productId provided for Item!");
            }
            if (item.getOrder() == null || item.getOrder().getId() == null && orderId == null) {
                order = createOrder(OrderStatus.IN_CART);
                orderDao.save(order);
                orderId = order.getId();
            }
            item.setOrder(order);
            itemDao.save(item);
            itemIds.add(item.getId());
        }
        response.put("cartId", Arrays.asList(order.getId()));
        response.put("itemId", itemIds);
        return response;
    }

    public void removeItemFromCart(Long id){
        itemDao.delete(id);
    }

    public void removeItemsFromCart(Long[] itemIds){
        List<Item> items = Arrays.asList(itemIds).stream().map(i -> itemDao.findOne(i)).collect(Collectors.toList());
        itemDao.delete(items);
    }

    public void removeAllItemsFromCart(Long cartId){
        Order cart = orderDao.findByIdAndStatus(cartId, OrderStatus.IN_CART);
        itemDao.delete(cart.getItems());
    }

    public void updateItemInCart(Item item) throws NotFoundException, ObjectNotFoundException, IllegalAccessException {
        if (item.getId() == null) {
            throw new NotFoundException("No itemId provided!");
        }
        Item existingItem = itemDao.findOne(item.getId());
        if(existingItem == null) {
            throw new ObjectNotFoundException(item, "No item with id: " + item.getId() + " to update!");
        }
        EntityMerger.mergeEntities(existingItem, item);
        itemDao.save(existingItem);
    }

    public Long setOrderFromCart(Long cartId, Long customerId, OrderStatus status) throws NotFoundException, ObjectNotFoundException {
        if (customerId == null) {
            throw new NotFoundException("No customerId provided");
        }
        Customer customer = customerDao.findOne(customerId);
        if (customer == null) {
            throw new ObjectNotFoundException(customer, "No customer with id: " + customerId + " found!");
        }
        Order order = orderDao.findOne(cartId);
        if (order == null) {
            throw new ObjectNotFoundException(order, "No cart with id: " + cartId + " found!");
        }

        order.setStatus((status == null) ? OrderStatus.PENDING: status);
        order.setCustomer(customer);
        orderDao.save(order);

        return order.getId();
    }

    public void updateOrder(Order order, Long customerId) throws ObjectNotFoundException, IllegalAccessException {
        Customer customer = new Customer();
        if (customerDao.findOne(customerId) == null) {
            throw new ObjectNotFoundException(customer, "No customer with id: " + customerId + " found!");
        } else {
            customer = customerDao.findOne(customerId);
            order.setCustomer(customer);
        }
        Order existingOrder = orderDao.findOne(order.getId());
        if (order == null) {
            throw new ObjectNotFoundException(existingOrder, "No order with id: " + order.getId() + " found!");
        }
        EntityMerger.mergeEntities(existingOrder, order);
        orderDao.save(order);
    }

    private Order createOrder(OrderStatus status) {
        Order order = new Order();
        if (status != null) {
            order.setStatus(status);
        }
        return order;
    }
}