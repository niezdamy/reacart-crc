/*
 * Copyright (c) 2018 Accenture
 * For purposes of Corporate Readiness Certificate Programme
 * All rights reserved
 */

package com.crc.service;

import com.crc.bean.CustomerBean;
import com.crc.bean.ItemBean;
import com.crc.bean.OrderBean;
import com.crc.bean.ProductBean;
import com.crc.bean.transformer.AbstractTransformer;
import com.crc.model.Customer;
import com.crc.model.Item;
import com.crc.model.Order;
import com.crc.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;
/**
 * @author mpichura
 */
@Service
public class TransformerProvider {
    @Autowired
    AbstractTransformer<ProductBean, Product> productTransformer;

    @Autowired
    AbstractTransformer<CustomerBean, Customer> customerTransformer;

    @Autowired
    AbstractTransformer<OrderBean, Order> orderTransformer;

    @Autowired
    AbstractTransformer<ItemBean, Item> itemTransformer;

    public CustomerBean setCustomerFromModel(Customer model){
        CustomerBean customerBean = customerTransformer.transformToBean(model);
        customerBean.getOrders().addAll(orderTransformer.transformToBeans(model.getOrders()));
        return customerBean;
    }

    public Customer setCustomerFromBean (CustomerBean bean) {
        Customer customer = customerTransformer.transformFromBean(bean);
        return customer;
    }

    public ProductBean setProductFromModel (Product model) {
        return productTransformer.transformToBean(model);
    }

    public Product setProductFromBean (ProductBean bean) {
        return productTransformer.transformFromBean(bean);
    }

    public OrderBean setOrderFromModel (Order model) {
        OrderBean orderBean = orderTransformer.transformToBean(model);
        if (model.getCustomer() != null) {
            orderBean.setCustomer(customerTransformer.transformToBean(model.getCustomer()));
        }
        orderBean.getItems().addAll(model.getItems().stream().map(i -> setItemFromModel(i, false)).collect(Collectors.toList()));
        return orderBean;
    }

    public Order setOrderFromBean (OrderBean bean) {
        return orderTransformer.transformFromBean(bean);
    }

    public ItemBean setItemFromModel (Item model, boolean includeOrder) {
        ItemBean itemBean = itemTransformer.transformToBean(model);
        if(includeOrder){
            itemBean.setOrder(orderTransformer.transformToBean(model.getOrder()));
        }
        itemBean.setProduct(productTransformer.transformToBean(model.getProduct()));
        return itemBean;
    }

    public Item setItemFromBean (ItemBean bean) {
        Item item = itemTransformer.transformFromBean(bean);
        if (bean.getOrder() != null) {
            item.setOrder(orderTransformer.transformFromBean(bean.getOrder()));
        }
        if (item.getProduct() != null) {
            item.setProduct(productTransformer.transformFromBean(bean.getProduct()));
        }
        return item;
    }

}