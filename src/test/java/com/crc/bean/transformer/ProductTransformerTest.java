/*
 * Copyright (c) 2018 Accenture
 * For purposes of Corporate Readiness Certificate Programme
 * All rights reserved
 */

package com.crc.bean.transformer;

import com.crc.bean.ProductBean;
import com.crc.model.Product;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;


public class ProductTransformerTest {

    @Test
    public void testTransformToBean () {
        Product model = new Product();
        model.setId(new Long("1"));
        model.setAvailableQuantity(new BigInteger("10"));
        model.setName("Test Product1");
        model.setPrice(new BigDecimal("5.99"));

        ProductBean productBean = new ProductTransformer().transformToBean(model);

        Assert.assertEquals(new Long("1"), productBean.getId());
        Assert.assertEquals(new BigInteger("10"), productBean.getAvailableQuantity());
        Assert.assertEquals("Test Product1", productBean.getName());
        Assert.assertEquals(new BigDecimal("5.99"), productBean.getPrice());
        Assert.assertTrue(productBean.getItems().size() == 0);
    }

    @Test
    public void testTransformFromBean () {
        ProductBean bean = new ProductBean();
        bean.setId(new Long("1"));
        bean.setAvailableQuantity(new BigInteger("10"));
        bean.setName("Test Product1");
        bean.setPrice(new BigDecimal("5.99"));

        Product productBean = new ProductTransformer().transformFromBean(bean);

        Assert.assertEquals(new Long("1"), productBean.getId());
        Assert.assertEquals(new BigInteger("10"), productBean.getAvailableQuantity());
        Assert.assertEquals("Test Product1", productBean.getName());
        Assert.assertEquals(new BigDecimal("5.99"), productBean.getPrice());
        Assert.assertTrue(productBean.getItems().size() == 0);

        ProductBean beanWithNullId = new ProductBean();
        beanWithNullId.setAvailableQuantity(new BigInteger("10"));
        beanWithNullId.setName("Test Product1");
        beanWithNullId.setPrice(new BigDecimal("5.99"));

        Product productBeanWithNullId = new ProductTransformer().transformFromBean(beanWithNullId);

        Assert.assertEquals(null, productBeanWithNullId.getId());
        Assert.assertEquals(new BigInteger("10"), productBeanWithNullId.getAvailableQuantity());
        Assert.assertEquals("Test Product1", productBeanWithNullId.getName());
        Assert.assertEquals(new BigDecimal("5.99"), productBeanWithNullId.getPrice());
        Assert.assertTrue(productBeanWithNullId.getItems().size() == 0);
    }
}
