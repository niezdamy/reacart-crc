/*
 * Copyright (c) 2018 Accenture
 * For purposes of Corporate Readiness Certificate Programme
 * All rights reserved
 */

package com.crc.bean.transformer;

import com.crc.bean.CustomerBean;
import com.crc.model.Customer;
import org.junit.Assert;
import org.junit.Test;

public class CustomerTransformerTest {

    public void testTransformToBean () {
        Customer model = new Customer();
        model.setId(new Long("1"));
        model.setName("Test Customer1");
        model.setFirstName("Test");
        model.setLastName("Customer1");
        model.setAddress("Street 1 City Country");
        model.setPhoneNb(new Long("123456789"));

        CustomerBean customerBean = new CustomerTransformer().transformToBean(model);

        Assert.assertEquals(new Long("1"), customerBean.getId());
        Assert.assertEquals("Test Customer1", customerBean.getName());
        Assert.assertEquals("Test", customerBean.getFirstName());
        Assert.assertEquals("Customer1", customerBean.getLastName());
        Assert.assertEquals("Street 1 City Country", customerBean.getAddress());
        Assert.assertEquals(new Long("123456789"), customerBean.getPhoneNb());
    }

    @Test
    public void testTransformFromBean () {
        CustomerBean bean = new CustomerBean();
        bean.setId(new Long("1"));
        bean.setFirstName("Test");
        bean.setLastName("Customer1");
        bean.setAddress("Street 1 City Country");
        bean.setPhoneNb(new Long("123456789"));

        Customer customer = new CustomerTransformer().transformFromBean(bean);

        Assert.assertEquals(new Long("1"), customer.getId());
        Assert.assertEquals("Test Customer1", customer.getName());
        Assert.assertEquals("Test", customer.getFirstName());
        Assert.assertEquals("Customer1", customer.getLastName());
        Assert.assertEquals("Street 1 City Country", customer.getAddress());
        Assert.assertEquals(new Long("123456789"), customer.getPhoneNb());
    }
}
