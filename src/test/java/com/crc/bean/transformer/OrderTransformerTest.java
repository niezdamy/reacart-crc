/*
 * Copyright (c) 2018 Accenture
 * For purposes of Corporate Readiness Certificate Programme
 * All rights reserved
 */

package com.crc.bean.transformer;

import com.crc.bean.OrderBean;
import com.crc.bean.OrderStatus;
import com.crc.model.Order;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;

public class OrderTransformerTest {
    public void testTransformToBean () {
        Order model = new Order();
        model.setId(new Long("1"));
        model.setStatus(OrderStatus.IN_CART);
        model.setAddress("Street 2 City Country");
        model.setTotalAmount(new BigDecimal("122.48"));

        OrderBean orderBean = new OrderTransformer().transformToBean(model);

        Assert.assertEquals(new Long("1"), orderBean.getId());
        Assert.assertEquals(OrderStatus.IN_CART, orderBean.getStatus());
        Assert.assertEquals("Street 2 City Country", orderBean.getAddress());
        Assert.assertEquals(new BigDecimal("122.48"), orderBean.getTotalAmount());
    }

    @Test
    public void testTransformFromBean () {
        OrderBean bean = new OrderBean();
        bean.setId(new Long("1"));
        bean.setStatus(OrderStatus.IN_CART);
        bean.setAddress("Street 2 City Country");
        bean.setTotalAmount(new BigDecimal("122.48"));

        Order order = new OrderTransformer().transformFromBean(bean);

        Assert.assertEquals(new Long("1"), order.getId());
        Assert.assertEquals(OrderStatus.IN_CART, order.getStatus());
        Assert.assertEquals("Street 2 City Country", order.getAddress());
        Assert.assertEquals(new BigDecimal("122.48"), order.getTotalAmount());
    }
}
