/*
 * Copyright (c) 2018 Accenture
 * For purposes of Corporate Readiness Certificate Programme
 * All rights reserved
 */

package com.crc.bean.transformer;

import com.crc.bean.ItemBean;
import com.crc.model.Item;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigInteger;

public class ItemTransfomerTest {

    public void testTransformToBean () {
        Item model = new Item();
        model.setId(new Long("1"));
        model.setOrderedQuantity(new BigInteger("1"));

        ItemBean itemBean = new ItemTransformer().transformToBean(model);

        Assert.assertEquals(new Long("1"), itemBean.getId());
        Assert.assertEquals(new BigInteger("1"), itemBean.getOrderedQuantity());
    }

    @Test
    public void testTransformFromBean () {
        ItemBean bean = new ItemBean();
        bean.setId(new Long("1"));
        bean.setOrderedQuantity(new BigInteger("1"));

        Item item = new ItemTransformer().transformFromBean(bean);

        Assert.assertEquals(new Long("1"), item.getId());
        Assert.assertEquals(new BigInteger("1"), item.getOrderedQuantity());
    }
}
